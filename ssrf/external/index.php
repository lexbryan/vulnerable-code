<?php

require_once('../_helpers/strip.php');

if (strlen($_GET['url']) < 1) {
  die('Usage: ?url=https://google.com/');
}

// Pass an unsanitized URL to the file_get_contents function,
// which is one of the vulnerable PHP functions to fetch a URL
// or file.
$contents = file_get_contents($_GET['url']);

echo $contents;
