#!/bin/bash

echo $1 > _tmp/strip

php -S 127.0.0.1:8080 -t open-redirect &

open 'http://127.0.0.1:8080/?next=https://hackerone.com'

wait
